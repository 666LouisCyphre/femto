## Femto -- open source code editor for C/C++ programmers, inspired by Nano

### Overwiew
The femto project was started to improve our programming skills.
However it solve problem of poor colorizing syntax in Nano editor.

A small convenience is that Femto base on BSD license,
so you can use it for own purpose, just mention us as authors beforehand.
Your program can be not even open source.

### How to compile and install Femto

Download Femto source code, then:
    tar xvzf femto-x.y.z.tar.gz,
    cd femto-x.y.z,
    make,
    mv femto /usr/bin

### Web Page
http://femtoproj.eu/

### Mailing Lists
For now it is just one mail for all use:
    femtoproj.contact@gmail.com

### Bug/Error Reports
To report bug/error please send email on femtoproj.contact@gmail.com and in title type Report

### Current Version
Current version of Femto editor is 
    1.0.0
    


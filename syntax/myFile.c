#include <stdio.h>

int main(void) {
	printf("Hello World!");
	return 0;
}

void variablesDefinitions(int var) {
	short a;
	short int a; 
	unsigned short a; 
	unsigned short int a;
	int a; 
	const int a;
	unsigned int a; 
	signed int a;
	long int a;
	long a;
	unsigned long int a;
	unsigned long a;
	long long int a;
	long long a;
	unsigned long long int a;
	unsigned long long a;
	float a;
	double a;
	long double a; 
	signed char a;
	unsigned char a; 
	char a;
	wchar_t a;
}

void ptrDefinitions(int* ptr) {
	short* a;
	short int* a; 
	unsigned short* a; 
	unsigned short int* a;
	int* a; 
	unsigned int* a; 
	signed int* a;
	long int* a;
	long* a;
	unsigned long int* a;
	unsigned long* a;
	long long int* a;
	long long* a;
	unsigned long long int* a;
	unsigned long long* a;
	float* a;
	double* a;
	long double* a; 
	signed char* a;
	unsigned char* a; 
	char* a;
	wchar_t* a;
}

void cDefinitions(int a) {
	const int b = 0;
}

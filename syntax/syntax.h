/* This class contain methods to colorize syntax. Supported languages:
 * - C
 * - C++
 * Also there are some bugs:
 * - not working multiline comments
 * - if some keyword is after "//" it has own color
 * - only one element (of relevant type) in line
 * - "m" before "//" if "//" begin line
 */
//TODO FEATURE: method to colorize if 0 endif
//TODO FEATURE: multiline commenting

#pragma once
#include <iostream>
#include <stdio.h>
#include <string>


#define CONST_INT 5
#define MACRO(arg1) ((arg1) + 1)

class ColorizeSyntax {
private:
	const std::string ENDC = "\033[0m";
	const std::string GRAY = "\033[1;30;40m";
	const std::string RED = "\033[1;31;40m";
	const std::string CYAN = "\033[1;36;40m";
	const std::string BLUE = "\033[1;34;40m";
	const std::string YELLOW = "\033[1;33;40m";
	const std::string PURPLE = "\033[1;35;40m";
	const std::string GREEN = "\033[1;32;40m";
	const std::string WHITE = "\033[1;37;40m";

	int *head = NULL;
	int *tail = nullptr;

	std::string variableTypes[13] = { 
	"unsigned ", "signed ", "int ",
	"wchar_t ", "char ", "long", " long", "short ",
	"float ", "double ", "void ",
	"const ", "string "
	};

	std::string preprocessorInstructions[13] = {
		"#include", "#define", "#undef",
		"#elif", "#else", "#endif", "#ifdef",
		"#ifndef", "#error", "#warning",
		"#line", "#pragma", "#if "
	};

	std::string loopKeywords[9] = {
		"if ", "else ", "else if",
		"for ",
		"switch", "case ", "default",
		"while ", "do "
	};

	std::string othersKeywords[21] = {
	"break", "continue", "try ", "catch ", "return ", "new ", "delete[]",
	"using ", "namespace", "this", " delete ", "nullptr", "reinterpret_cast",
	"unique_ptr", "shared_ptr", "weak_ptr", "auto_ptr", "stack ", "priority_queue ", 
	"queue ", "vector "
	};

	std::string classKeywords[4] = {
		"class ", "private", "protected", "public"
	};

	std::string structUnionEnum[3] = {
		"struct ", "union ", "enum "
	};

	std::string constants[5] = { 
		"NULL", "FILENAME_MAX", "EXIT_FAILURE",
		"EXIT_SUCCESS", "RAND_MAX", 
	};

	std::string changeElemmentsOfSyntax(std::string dataToChange, std::string *pointerToTable, int size, std::string color);
	std::string replace(std::string dataToColorize, std::string name, int size, std::string color);

public:
	std::string colorizedData(std::string data);
	std::string comment(std::string comment);
	std::string quotationMarks(std::string quotationMarks);
	std::string preprocessor(std::string preprocessor);
	std::string constantsAndMacros(std::string preprocessor);
	std::string predefinedConstants(std::string predefined);
	std::string loops(std::string loop);
	std::string classes(std::string classes);
	std::string structuresUnionsEnums(std::string sUE);
	std::string others(std::string others);
	std::string variables(std::string variables); 
};

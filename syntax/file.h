/* This class contain methods to read/write to .c .cpp and .txt file
 */
#pragma once
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include "syntax.h"

class File :public ColorizeSyntax {
private:

protected:
	std::fstream file;

public:
	std::string fileRead(std::string fileName);
	void fileWrite(std::string fileName, std::string data);
#if 0
	//this methods will be aviable in future
	void fileClear();
	void fileFind();
#endif
};

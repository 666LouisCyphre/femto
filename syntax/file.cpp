#include <iostream>
#include "file.h"

std::string File::fileRead(std::string fileName) {
	using namespace std;
	string curLine = "";
	string line = "";
	file.open(fileName, ios::in);
	if (file.good() == 0) return "e";//cout << "File can not be open" << endl;
	else {
		while (getline(file, curLine)) { 
			curLine = colorizedData(curLine);
			line += curLine;
			line += "\n";
		}
	}
	file.close();

	return line;
}

void File::fileWrite(std::string fileName, std::string data) {
	using namespace std; 
	file.open(fileName, ios::out | ios::app);
	file << data;
	file.close();
}



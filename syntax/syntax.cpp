#include <iostream>
#include "syntax.h"

std::string ColorizeSyntax::colorizedData(std::string data) {
	std::string outputData = "";
	outputData += preprocessor(data); //all assignment
	outputData = constantsAndMacros(outputData);
	outputData = predefinedConstants(outputData);
	outputData = comment(outputData);
	outputData = quotationMarks(outputData);
	outputData = loops(outputData);
	outputData = classes(outputData);
	outputData = structuresUnionsEnums(outputData);
	outputData = others(outputData);
	outputData = variables(outputData);
	return outputData;
}

std::string ColorizeSyntax::constantsAndMacros(std::string preprocessor) {
	int i = 0;
	int j = 0;
	int lng = 0;
	int size = preprocessor.length();
	std::string constant = "";
	for (i; i < size; ++i) {
		if (isupper(preprocessor[i]) && isupper(preprocessor[i + 1]) && !islower(preprocessor[i - 1])) {
			j = i;
			for (j; j < size; ++j) {
				if (isupper(preprocessor[j])) {
					constant += preprocessor[j];
					continue;
				}
				else if (preprocessor[j] == '_' || preprocessor[j] == '-') {
					constant += preprocessor[j];
					continue;
				}
				else if (preprocessor[j] == ' ' || preprocessor[j] == ')' || preprocessor[j] == '('  || preprocessor[j] == ']' || preprocessor[j] == '>') break;
				else {
					constant = "";
					break;
				}
			}
			break;
		}
	}
	lng = constant.length();
	preprocessor = replace(preprocessor, constant, lng, RED);
	return preprocessor;
}

std::string ColorizeSyntax::comment(std::string comment) {
	int index = 0;
	int i = 0;
	int size = comment.length();
	std::string commentedText = "";
	for (i; i < size; ++i) {
		if (comment[i] && comment[i + 1] == '/') {
			index = i;
			for (index; index < size; ++index) commentedText += comment[index];
			break;
		}
	}
	int cTsize = commentedText.length();
	std::string colorizedComment = replace(comment, commentedText, cTsize, GREEN);
	return colorizedComment;
}

std::string ColorizeSyntax::quotationMarks(std::string quotationMarks) {
	//<>""'' YELLOW
	int i = 0;
	int j = 0; 
	int lng = 0;
	const char SingleQuote = 39;
	const char Quote = 34;
	int size = quotationMarks.length();
	std::string text = "";
	for (i; i < size; ++i) {
		if (quotationMarks[i] == Quote || quotationMarks[i] == SingleQuote || quotationMarks[i] == '<') {
			if (quotationMarks[i] && quotationMarks[i + 1] == '<') {
				i++;
				continue;
			}
			j = i + 1;
			text += quotationMarks[i];
			for (j; j < size; ++j) {
				text += quotationMarks[j];
				int closeMark = quotationMarks.find(">");
				if (quotationMarks[j] == Quote || quotationMarks[j] == SingleQuote || quotationMarks[j] == '>') break;
				if (quotationMarks[i] == '<' && closeMark == std::string::npos){
					text = "";
					continue;
				}
			}
			break;
		}
	}
	lng = text.length();
	quotationMarks = replace(quotationMarks, text, lng, YELLOW);
	return quotationMarks;
}

std::string ColorizeSyntax::replace(std::string dataToColorize, std::string name, int size, std::string color) {
	try {
		std::string colorizedData = dataToColorize.replace(dataToColorize.find(name), size, color + name + ENDC);
		return colorizedData;
	}
	catch (std::out_of_range ) {
		return dataToColorize;
	}
}

std::string ColorizeSyntax::changeElemmentsOfSyntax(std::string dataToChange, std::string *pointerToTable, int size, std::string color) {
	int i = 0;
	for (i; i < size; ++i) {
		std::string tabElement = *(pointerToTable + i);
		int lng = tabElement.length();
		dataToChange = replace(dataToChange, tabElement, lng, color);
		
	}
	return dataToChange;
}

std::string ColorizeSyntax::preprocessor(std::string preprocessor) {
	preprocessor = changeElemmentsOfSyntax(preprocessor, preprocessorInstructions, 13, RED);
	return preprocessor;
}

std::string ColorizeSyntax::predefinedConstants(std::string predefined) {
	predefined = changeElemmentsOfSyntax(predefined, constants, 5, RED);
	return predefined;
}

std::string ColorizeSyntax::loops(std::string loop) {
	loop = changeElemmentsOfSyntax(loop, loopKeywords, 9, PURPLE);
	return loop;
}

std::string ColorizeSyntax::classes(std::string classes) {
	classes = changeElemmentsOfSyntax(classes, classKeywords, 4, PURPLE);
	return classes;
}

std::string ColorizeSyntax::structuresUnionsEnums(std::string sUE) {
	sUE = changeElemmentsOfSyntax(sUE, structUnionEnum, 3, PURPLE);
	return sUE;
}

std::string ColorizeSyntax::others(std::string others) {
	others = changeElemmentsOfSyntax(others, othersKeywords, 21, PURPLE);
	return others;
}

std::string ColorizeSyntax::variables(std::string variables) { 
	variables = changeElemmentsOfSyntax(variables, variableTypes, 13, CYAN);
	return variables;
}



#include <iostream>
#include <string>
#include "syntax.h"
#include "file.h"

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77
#define BACKSPACE 8
#define ENTER 13
#define Q 81

//TODO: include and implement ncurses to use moving with arrows

int main(int argc, char *argv[]) {
	using namespace std;
#if 0
	string data = "";
	char c = 0;
	bool exit = false;
	char choice = 0;

	cout << "Give string to write:\n";
	do {
		while ((c = _getch()) != Q) { // what is substitute for _getch() in linux?
			if (c == ENTER) {
				cout << "\n";
				data += "\n";
			}
			else if (c == KEY_LEFT) {
				//cout << "\bThe Number of the Beast\a";
				cout << "\b\a";
			}
			else if (c == KEY_RIGHT) {
				//cout << "\b is 666\a";
				cout << " \a";
			}
			else if (c == BACKSPACE) {
				cout << "\b \b"; //space overwrite character
				data.erase(data.length() - 1, 1);
			}
			else {
				cout << c;
				data += c;
			}
		}
		if (c == Q) {
			cout << "Do you want to save and exit? Y/n: ";
			cin >> choice;
			exit = choice == 'Y' ? true : false;
		}
	} while (exit == false);

	cout << endl << "----------------------------------------" << endl << endl;

	cout << "Colorized output equal:" << "\033[1;31;40m HELLO WORLD!!! \033[0m" << "than again normal text \n";
	File* file = new File();
	cout << "Your file: \n\n";
	std::string name = "myFile.txt";
	std::string fileO = file->fileRead(name);
	cout << fileO;
	cout << "\nAnd the end of file\n";
	file->fileWrite(name, data);
	cout << "Writted data into file:\n";
	fileO = file->fileRead(name);
	cout << fileO;
	cout << "\nAnd the end of file\n";
#endif
	File* file = new File();
	ColorizeSyntax* colorizeSyntax = new ColorizeSyntax();
	std::string name = "myFile.h";
	std::string fileO = file->fileRead(name);
	//here class ColorizeSyntaxt must receive data from fileRead method
	//colorizeSyntax->colorizedData(fileO);
	//and than arrow functions receive strind from ColorizeSyntax
	cout << fileO;

	if (argc != 2) cout << "[!] Bad usage type -h for help";
	else {
		std::string name = *(argv + 1);
		std::string fileO = file->fileRead(name);
		if(fileO[0] == 'e') {
			//just for tests, replace when GUI will be ready
			cout << "Give one line of code";
			std::string data = "";
			getline(cin, data);
			file->fileWrite(name, data);
		}
		else cout << fileO;
	}

	getchar();
	getchar();
	return 0;
}
